import sys
import time

delta = -4
scores = {}


def init(stdin):
    letters = stdin.readline().split()

    for l1 in letters:
        line = list(map(int, stdin.readline().split()))
        for i in range(len(letters)):
            scores[l1, letters[i]] = line[i]

    query_amount = int(stdin.readline())

    queries = [None] * query_amount
    for i in range(query_amount):
        q = stdin.readline().split()
        queries[i] = (q[0], q[1])

    return queries


def make_table(x, y):
    table = dict()

    for i in range(x + 1):
        table[i, 0] = i * delta

    for j in range(y + 1):
        table[0, j] = j * delta

    return table


def make_pred_table(x, y):
    table = dict()

    for i in range(x + 1):
        table[i, 0] = i - 1, 0

    for j in range(y + 1):
        table[0, j] = 0, j - 1

    table[0, 0] = None

    return table


def find_path(preds, x, y):
    path = [(x, y)]

    pred = preds[x, y]
    while pred is not None:
        path.append(pred)
        pred = preds[pred]

    return list(reversed(path))


def path_to_strings(path, w1, w2):
    string1 = ""
    string2 = ""

    for i in range(len(path) - 1):
        if path[i][0] < path[i + 1][0]:
            string1 += w1[i]
        else:
            string1 += '*'
            w1 = string1 + w1[i:]

        if path[i][1] < path[i + 1][1]:
            string2 += w2[i]
        else:
            string2 += '*'
            w2 = string2 + w2[i:]

    return string1, string2


def algorithm(query):
    w1, w2 = query

    x, y = len(w1), len(w2)

    table = make_table(x, y)
    preds = make_pred_table(x, y)

    s_a = time.time()
    for i in range(x):
        for j in range(y):
            OPT(w1, w2, i + 1, j + 1, table, preds)
    print('the algorithm took: ' + str(time.time() - s_a) + ' seconds')

    s1, s2 = path_to_strings(find_path(preds, x, y), w1, w2)

    return s1, s2


def OPT(w1, w2, i, j, table, preds):

    if (i, j) not in table:
        opt, pred = max(
            (OPT(w1, w2, i - 1, j, table, preds) + delta, (i - 1, j)),
            (OPT(w1, w2, i, j - 1, table, preds) + delta, (i, j - 1)),
            (OPT(w1, w2, i - 1, j - 1, table, preds) + scores[w1[i - 1], w2[j - 1]], (i - 1, j - 1))
        )

        table[i, j] = opt
        preds[i, j] = pred

    return table[i, j]


def main():
    queries = init(sys.stdin)

    s = time.time()
    for q in queries:
        w1, w2 = algorithm(q)
        print(w1 + ' ' + w2)
    print('In total it took ' + str(time.time() - s) + ' seconds')


if __name__ == '__main__':
    main()
