import sys
import collections
import time


# start = time.time()
# end = time.time()
# print((end - start)*1000)

class Male:
    def __init__(self, idnr, preferencelist):
        self.idnr = idnr
        self.preferencelist = preferencelist


class Female:
    def __init__(self, idnr, preferencelist, n):
        self.idnr = idnr
        self.preferencelist = self.setpref(preferencelist, n)
        self.match = None

    def setmatch(self, match):
        self.match = match

    def setpref(self, preferencelist, n):
        tmp = list(range(n))
        for i in range(len(preferencelist)):
            tmp[preferencelist[i]-1] = i + 1

        return tmp

    def comparepreference(self, challenger):
        if self.preferencelist[self.match.idnr - 1] > self.preferencelist[challenger - 1]:
            return True
        else:
            return False


def initialize(stdin):
    n = int(stdin.readline())
    data = list(map(int, stdin.read().split()))
    males = collections.deque()
    females = list(range(n))
    femaleids = {-1}

    loop = 0

    for i in range(2 * n):
        id = data[loop]
        loop += 1
        preferences = collections.deque()

        for x in range(n):
            preferences.append(data[loop])
            loop += 1

        if not idinlist(id, femaleids):
            females[id-1] = Female(id, preferences, n)
            femaleids.add(id)

        else:
            males.append(Male(id, preferences))
    return [males, females]


def idinlist(idnr, ids):
    return idnr in ids


class GaleShapley:

    def __init__(self, list):
        self.males = list[0]
        self.females = list[1]
        self.pairs = {}  # kvinna till man

    def run(self):

        while self.males:
            m = self.males.popleft()
            w = m.preferencelist.popleft()

            if not (w in self.pairs):
                self.pairs[w] = m
                self.females[w-1].setmatch(m)
            elif self.females[w-1].comparepreference(m.idnr):
                mw = self.pairs.pop(w)
                self.pairs[w] = m
                self.females[w-1].setmatch(m)
                self.males.append(mw)
            else:
                self.males.append(m)
        return self.pairs
# Algorithm
# procedure GS (W , M)
#   /* W is a set of n women and M is a set of n men */
#   add each man m ∈ M to a list p
#   while p != null
#       m ← take out the first element from p
#       w ← the woman m prefers the most and
#           m has not yet proposed to
#       if w has no partner then
#           (m,w) becomes a pair
#       else if w prefers m over her current partner mw then
#           remove the pair (mw ,w)
#           (m,w) becomes a pair
#           add mw to p
#       else
#           add m to p


def main():

    start = time.time()
    gs = GaleShapley(initialize(sys.stdin))
    start = time.time()
    pairs = gs.run()

    for w in pairs:
        print(pairs[w].idnr)

    end = time.time()
    print(end - start)


if __name__ == '__main__':
    main()


