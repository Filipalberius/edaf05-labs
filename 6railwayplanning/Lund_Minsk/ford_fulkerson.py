import sys
from collections import deque


def init(stdin):
    node_amount, edge_amount, target_flow, plan_size = tuple(map(int, stdin.readline().split()))

    graph = [set() for _ in range(node_amount)]
    graph_list = [i for i in range(edge_amount)]
    capacity_flow = dict()

    for i in range(edge_amount):
        start, end, capacity = tuple(map(int, stdin.readline().split()))

        graph[start].add(end)
        graph[end].add(start)

        graph_list[i] = start, end

        capacity_flow[start, end] = capacity, 0
        capacity_flow[end, start] = capacity, 0

    plan_indices = [int(stdin.readline()) for _ in range(plan_size)]
    plan = deque([graph_list[i] for i in plan_indices])

    return graph, plan, capacity_flow, target_flow


def make_safe_graph(graph, plan):
    for p in plan:
        graph[p[0]].remove(p[1])
        graph[p[1]].remove(p[0])

    return graph


def algorithm(graph, plan, capacity_flow, target_flow):
    graph = make_safe_graph(graph, plan)

    current_flow = ford_fulkerson(graph, capacity_flow, 0)

    while current_flow < target_flow:
        capacity = 0

        while capacity < target_flow - current_flow:
            s, e = plan.pop()

            graph[s].add(e)
            graph[e].add(s)

            c = capacity_flow[s, e][0]
            capacity += c

        current_flow = ford_fulkerson(graph, capacity_flow, current_flow)

    return len(plan), current_flow


def ford_fulkerson(graph, capacity_flow, init_flow):
    current_flow = init_flow

    while True:
        path = bfs(graph, capacity_flow, 0, len(graph) - 1)

        if not path:
            return current_flow

        deltas = [None for _ in range(len(path) - 1)]
        for i in range(len(path) - 1):
            capacity, flow = capacity_flow[path[i], path[i+1]]
            delta = capacity - flow
            deltas[i] = delta

        min_delta = min(deltas)

        for i in range(len(path) - 1):
            capacity, old_flow = capacity_flow[path[i], path[i + 1]]
            new_flow = old_flow + min_delta
            capacity_flow[path[i], path[i + 1]] = capacity, new_flow
            capacity_flow[path[i + 1], path[i]] = capacity, new_flow

        current_flow = current_flow + min_delta


def make_path(preds, target, source):
    path = deque([target])
    x = target
    while x != source:
        x = preds[x]
        path.append(x)
    return list(path)


def bfs(graph, capacity_flow, source, target):
    q = deque()
    q.append(source)
    visited = {source}
    preds = dict()

    while q:
        node_1 = q.popleft()
        neighbors = graph[node_1]
        for node_2 in neighbors:
            capacity, flow = capacity_flow[node_1, node_2]
            delta = capacity - flow
            if node_2 not in visited and delta is not 0:
                visited.add(node_2)
                q.append(node_2)
                preds[node_2] = node_1
                if node_2 == target:
                    return make_path(preds, node_2, source)
    return []


def main():
    graph, plan, capacity_flow, target_flow = init(sys.stdin)
    plans_used, flow = algorithm(graph, plan, capacity_flow, target_flow)
    print(str(plans_used) + ' ' + str(flow))


if __name__ == '__main__':
    main()
