import sys
from collections import deque
import time


def init(stdin):

    node_amount, edge_amount = list(map(int, stdin.readline().split()))
    data = list(map(int, stdin.read().split()))

    nodes = [None] * node_amount

    for i in range(node_amount):
        nodes[i] = Node(i + 1)

    edges = []
    for i in range(edge_amount):
        weight = data[3*i + 2]
        n1 = nodes[data[(3*i)] - 1]
        n2 = nodes[data[(3*i) + 1] - 1]
        edge = Edge(weight, n1, n2)
        edges.append(edge)

    edges.sort()
    return edges


class Node:
    def __init__(self, id):
        self.id = id
        self.parent = self
        self.size = 1


class Edge:
    def __init__(self, weight, n1, n2):
        self.weight = weight
        self.n1 = n1
        self.n2 = n2

    def __lt__(self, other):
        return self.weight < other.weight


def find(node):
    p = node
    while p.parent is not p:
        p = p.parent
    while node.parent is not node:
        w = node.parent
        node.parent = p
        node = w
    return node


def union(n1, n2):
    if n1.size < n2.size:
        n1.parent = n2
        n2.size += n1.size
    else:
        n2.parent = n1
        n2.size += n1.size


def kruskal(edges):
    total_weight = 0
    edges = deque(edges)

    while edges:
        e = edges.popleft()
        n1_parent = find(e.n1)
        n2_parent = find(e.n2)
        if n1_parent is not n2_parent:
            union(n1_parent, n2_parent)
            total_weight += e.weight

    return total_weight


def main():
    total_s = time.time()

    init_s = time.time()
    graph = init(sys.stdin)
    init_e = time.time()

    kruskal_s = time.time()
    print(kruskal(graph))
    kruskal_e = time.time()

    total_e = time.time()

    print('init took: ' + str(init_e - init_s) + ' seconds')
    print('kruskals took: ' + str(kruskal_e - kruskal_s) + ' seconds')
    print('total time was: ' + str(total_e - total_s) + ' seconds')


if __name__ == '__main__':
    main()