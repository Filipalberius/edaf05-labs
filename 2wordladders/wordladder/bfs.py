import sys
import time
from collections import deque


def makecombos(word):
    combos = []
    for i in range(len(word)):
        combos.append(''.join(sorted(word[:i] + word[(i + 1):])))

    return list(dict.fromkeys(combos))


def initgraph(words):

    graph = {}

    for word in words:
        combos = makecombos(word)
        for combo in combos:
            if combo not in graph:
                graph[combo] = []
            graph[combo].append(word)
    print(graph)

    return graph


def initialize(stdin):
    configs = stdin.readline().split()
    wordammount = int(configs[0])
    queryammount = int(configs[1])
    data = stdin.read().split()

    words = data[:wordammount]
    data = data[wordammount:]
    queries = []

    for i in range(queryammount):
        queries.append((data[2*i], data[2*i+1]))

    return words, queries


def bfs(graph, source, target):
    counter = {}

    if source == target:
        print(0)
        return

    q = deque()
    q.append(source)
    visited = {source}
    counter[source] = 0
    while q:
        v = q.popleft()
        key = ''.join(sorted(v[1:]))
        neighbors = graph[key]
        for word in neighbors:
            if word not in visited:
                visited.add(word)
                counter[word] = counter[v] + 1
                q.append(word)
                if word == target:
                    print(counter[word])
                    return

    print("Impossible")


# procedure BFS (G,s,t)
#     q ← new list containing s
#     for v ∈ V
#         visited(v) ← 0
#     visited(s) ← 1
#     while q != null
#         v ← take out the first element from q
#         for w ∈ neighbor(v)
#             if not visited(w) then
#                 visited(w) ← 1
#                 add w to end of q
#                 pred(w) ← v #vi ska counter istället!
#                 if w = t then
#                     print ”found path s − t”
#                     return
# print ”found no path s − t”


def main():
    startinit = time.time()
    words, queries = initialize(sys.stdin)
    endinit = time.time()

    #print("init done in: " + str(endinit - startinit) + " seconds")

    startgraph = time.time()
    graph = initgraph(words)
    endgraph = time.time()

    #print("graph made in: " + str(endgraph - startgraph) + " seconds")

    startpath = time.time()

    for q in queries:
        bfs(graph, q[0], q[1])

    endpath = time.time()
    #print("paths found in: " + str(endpath - startpath) + " seconds")


if __name__ == '__main__':
    main()
