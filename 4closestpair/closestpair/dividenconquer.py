import sys
import math
import time


class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def distance_to(self, other):
        dx = abs(self.x - other.x)
        dy = abs(self.y - other.y)
        return math.sqrt((dx ** 2) + (dy ** 2))


def init(stdin):
    point_amount = int(stdin.readline())
    data = list(map(int, stdin.read().split()))

    pts = [None] * point_amount

    for i in range(point_amount):
        pts[i] = Point(data[2 * i], data[2 * i + 1])

    return pts


def divide(p_x, p_y):
    l_x = p_x[:len(p_x) // 2]
    r_x = p_x[len(p_x) // 2:]
    set_lx = set(l_x)
    set_rx = set(r_x)

    l_y = []
    for p in p_y:
        if p in set_lx:
            l_y.append(p)
    r_y = []
    for p in p_y:
        if p in set_rx:
            r_y.append(p)

    return l_x, l_y, r_x, r_y


def make_s(p_y, delta, xp):
    s = []
    for p in p_y:
        if xp - delta < p.x < xp + delta:
            s.append(p)

    return s


def closest_path(pts):
    p_x = sorted(pts, key=lambda p: p.x)
    p_y = sorted(pts, key=lambda p: p.y)
    result = closest(p_x, p_y, len(pts))

    return result


def closest(p_x, p_y, n):
    if n == 2:
        return p_x[0].distance_to(p_x[1])

    elif n == 3:
        d_1 = p_x[0].distance_to(p_x[1])
        d_2 = p_x[1].distance_to(p_x[2])
        d_3 = p_x[2].distance_to(p_x[0])

        return min([d_1, d_2, d_3])

    else:
        l_x, l_y, r_x, r_y = divide(p_x, p_y)

        delta = min(closest(r_x, r_y, len(r_x)), closest(l_x, l_y, len(l_x)))
        xp = r_x[0].x
        s = make_s(p_y, delta, xp)

        for i in range(len(s)):
            for j in range(7):  # XXXX
                if (i + j + 1) >= len(s):
                    break

                distance = s[i].distance_to(s[i + j + 1])

                if distance < delta:
                    delta = distance

        return delta


def main():

    # s_alg = time.time()
    print("%.6f" % closest_path(init(sys.stdin)))
    # e_alg = time.time()
    # print("the total time of running the algorithm is: " + str(e_alg - s_alg) + " seconds")


if __name__ == '__main__':
    main()
